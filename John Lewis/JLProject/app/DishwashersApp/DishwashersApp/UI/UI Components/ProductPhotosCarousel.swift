//
//  ProductPhotosCarousel.swift
//  DishwashersApp
//
//  Created by Marcin Maciukiewicz on 24/05/2017.
//  Copyright © 2017 Blue Pocket Limited. All rights reserved.
//

import UIKit

@IBDesignable class ProductPhotosCarousel: DesignableView {

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pager: UIPageControl!
 
//    fileprivate var productImageViews:[UIImageView] = []
}

extension ProductPhotosCarousel {
    
    func loadImages(_ productImages: [URL], withImagesProvider imageProvider: ImagesProvider) {

        scrollView.subviews.forEach {$0.removeFromSuperview()}
        scrollView.backgroundColor = UIColor.red

        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        scrollView.scrollsToTop = false
        scrollView.delegate = self

        let numberOfImages = productImages.count
        pager.numberOfPages = numberOfImages
        pager.currentPage = 0

        let scrollBoundsSize = scrollView.bounds.size
        let pageWidth = scrollBoundsSize.width
        let contentWidth = pageWidth * CGFloat(numberOfImages)
        let contentHeight = scrollBoundsSize.height
        scrollView.contentSize = CGSize(width: contentWidth, height: contentHeight)
        

        
        (0..<numberOfImages).forEach { (pageIndex:Int) in
            
            let imageXOffset = pageWidth * CGFloat(pageIndex)
            let imageFrame = CGRect(x: imageXOffset, y: 0, width: 10, height: 10)
            let imageView = UIImageView(frame: imageFrame)
            scrollView.addSubview(imageView)

            imageView.setImage(UIImage(named:"image_placeholder")!)
//            let imageURL = productImages[pageIndex]
//            imageProvider(imageURL) { (productImage: UIImage?) in
//                
//                guard let img = productImage else { return }
//                
//                DispatchQueue.main.async {
//                    // Use the method from extension to reduce memory footprint
//                    imageView.setImage(img)
//                }
//            }
        }
        
        layoutIfNeeded()
        
    }
    
    override func layoutSubviews() {
        
//        let productImageSubviews = scrollView.subviews
//        let numberOfImages = productImageSubviews.count
//        
//        let scrollBoundsSize = scrollView.bounds.size
//        let pageWidth = scrollBoundsSize.width
//        let contentHeight = scrollBoundsSize.height
//        
//        (0..<numberOfImages).forEach { (pageIndex:Int) in
//            let imageXOffset = pageWidth * CGFloat(pageIndex)
//            let imageFrame = CGRect(x: imageXOffset, y: 0, width: pageWidth, height: contentHeight)
//            let imageView = productImageSubviews[pageIndex]
//            imageView.frame = imageFrame
//        }
        
    }
}


extension ProductPhotosCarousel : UIScrollViewDelegate {
 
    public func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        let pageWidth = self.scrollView.bounds.width
        let pageIndex = floor((self.scrollView.contentOffset.x - pageWidth / 2) / pageWidth) + 1
        pager.currentPage = Int(pageIndex)
        
    }
    
}
