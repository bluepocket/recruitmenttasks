# John Lewis Partnership
iOS Developer Technical Test – Dishwasher App


## Introduction

In this pack you will find:

* This document
* 3 images that show the desired screen layout for the app
* Product Page Portrait.png
* Product Page Landscape.png
* Product Grid.png
* ProductGridAPI
* ProductPageAPI


The aim of this test is for the applicant to write an iOS Application that sells Dishwashers.

We would like to see the following

* The App should work on an iPad, in landscape and portrait mode.
* The code should be written in Swift 3.
* The use of third party Code/SDKs is allowed, but you should be able to explain why you have chosen the third party.
* We’d like to see a TDD approach to writing the app, using XCTest.
* When submitting your app to us, we expect the code to build and run using the latest version of Xcode that is currently available in the Apple App Store.
* Committing your code constantly to a GitHub account.
* Putting all your assumptions, notes and instructions into your GitHub README.md.

We’re not looking for a ‘pixel perfect’ app, we are looking at your style of coding, and how you’ve approached this.

## Brief
Your task is to write an app that will allow customers to see the range of dishwashers John Lewis sells. This app will be a simple to use and will make use of existing John Lewis APIs.

## Product Grid
When the app is launched, the customer will be presented with a grid of dishwashers that are currently available for customers to buy. For this exercise we’d be looking at only displaying the first 20 results returned by the API.

The layout of the screen should look like the attached image - Product Grid.png

Details of the API Can be found in the document ProductGridAPI

## Product Page - (Optional)
When a dishwasher is tapped, a new screen is displayed showing the dishwasher’s details. The screen layout is defined in the following two files

* Portrait Mode -  Product Page Portrait.png
* Landscape Mode - Product Page Landscape.png

Details of the API Can be found in the attached document ProductPageAPI

We’d like to see the Product page display the details for the product page, we don’t expect you to provide a further solution to drill down to the reviews, or add to basket functionality.

## Submitted task
The submitted task [is here](JLProject)