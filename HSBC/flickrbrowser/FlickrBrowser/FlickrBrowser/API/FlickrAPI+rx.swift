//
//  FlickrAPI+rx.swift
//  FlickrBrowser
//
//  Created by Marcin Maciukiewicz on 04/06/2017.
//  Copyright © 2017 Marcin Maciukiewicz. All rights reserved.
//

import Foundation
import RxSwift

extension Reactive where Base: FlickrAPI {
    
    public var allPublicPhotos: Observable<FlickrFeed> {
        return getPublicPhotos(query: PublicPhotosAPIQuery.empty)
    }
    
    public func getPublicPhotos(query: PublicPhotosAPIQuery) -> Observable<FlickrFeed> {
        
        return Observable.create { (observer: AnyObserver<FlickrFeed>) -> Disposable in
            
            let disposeBag = DisposeBag()
            Observable
                .just(query)
                .map({ (query: PublicPhotosAPIQuery) -> [String:String] in
                    return query.queryParams
                })
                .map({ (queryParams: [String : String]) -> URL in
                    let endpointURL = self.base.config.createEndpointURL(service: .publicPhotos, queryParams: queryParams)
                    return endpointURL
                })
                .map({ (serviceURL: URL) -> NetworkOperationBlock in
                    self.base.config.networkProvider.createGETOperation(url: serviceURL, operationResult: { (operationStatus: NetworkOperationStatus) in
                        FlickrAPI.rx.retrievedPublicPhotos(observer: observer, status: operationStatus)
                    })
                })
                .subscribe(onNext: { (operation: @escaping NetworkOperationBlock) in
                    self.base.config.networkExecutor.execute(operation: operation)
                }).disposed(by: disposeBag)
            
            
            return Disposables.create()
        }
        
    }

    fileprivate static func retrievedPublicPhotos(observer: AnyObserver<FlickrFeed>, status: NetworkOperationStatus) {
        switch status {
        case .successful(let data):
            if let flickrFeed = FlickrFeed.parse(feedAsJsonData: data) {
                observer.onNext(flickrFeed)
            } else {
                observer.onError(FlickrAPIError.jsonDeserialisationError)
            }
        case .failed(let error):
            let err = error ?? FlickrAPIError.unknownError
            observer.onError(err)
            return
        }
        observer.onCompleted()
    }
}

public class RxFlickrAPI {
    
    
    public var publicPhotos: Observable<FlickrFeed>
    
    private let api:FlickrAPI
    private let internalPublicPhotos:PublishSubject<FlickrFeed>
    private let disposeBag = DisposeBag()
    
    public required init(_ api:FlickrAPI) {
        self.api = api
        internalPublicPhotos = PublishSubject<FlickrFeed>()
        publicPhotos = internalPublicPhotos.shareReplay(1)
    }
    
    public func fetchPublicPhotos(query: PublicPhotosAPIQuery) {
        
        Observable
            .just(api,query)
            .map({ (input: (FlickrAPI, PublicPhotosAPIQuery)) -> (FlickrAPI, [String:String]) in
                let (api, query) = input
                return (api, query.queryParams)
            })
            .map({ (input: (FlickrAPI, [String:String])) -> (FlickrAPI, URL) in
                let (api, queryParams) = input
                let endpointURL = api.config.createEndpointURL(service: .publicPhotos, queryParams: queryParams)
                return (api, endpointURL)
            })
            .map({ (input: (FlickrAPI, URL)) -> (FlickrAPI, NetworkOperationBlock) in
                let (api, serviceURL) = input
                let result = api.config.networkProvider.createGETOperation(url: serviceURL, operationResult: { (operationStatus: NetworkOperationStatus) in
                    
                    switch operationStatus {
                    case .successful(let data):
                        if let flickrFeed = FlickrFeed.parse(feedAsJsonData: data) {
                            self.internalPublicPhotos.onNext(flickrFeed)
                        } else {
                            self.internalPublicPhotos.onError(FlickrAPIError.jsonDeserialisationError)
                        }
                    case .failed(let error):
                        let err = error ?? FlickrAPIError.unknownError
                        self.internalPublicPhotos.onError(err)
                        return
                    }
                })
                return (api, result)
            })
            .subscribe(onNext: { (input: (FlickrAPI, NetworkOperationBlock)) in
                let (api, operation) = input
                api.config.networkExecutor.execute(operation: operation)
            }).disposed(by: disposeBag)
        
    }
}
