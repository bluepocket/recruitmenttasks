# Technical Task

## BriefingWe would like you to implement an image gallery application (iOS or Android) that uses the public image Flickr feed as a data source. Requirements:* Use the most recent tools for the platform of your choice (iOS – Swift/Xcode or Android - Java /Android Studio or Android Java/Kotlin/Android Studio)* Limit your usage of 3rd party libraries only to the few ones that add a large benefit to the architecture and testability of the project. Good example of libraries that could be used are RxSwift, RxJava etc.* Flickr url that should be used is: https://www.flickr.com/services/feeds/docs/photos_public* Image metadata should be visible for each picture* Git should be used as version control and to track the application developmentOptional* Search for images by tag* Image caching* Order by date taken or date published* Save image to the System Gallery* Open image in system browser* Share picture by email
 Output:
* Provide a link to the Github or Bitbucket repository with a project.References* JSON feed documentation: https://www.flickr.com/services/api/response.json.html
 You are not required to deliver an end-to-end application or all of the above features. Prioritise based on your experience. We are looking for production code quality, not quantity. Please come up with your own UI & UX keeping in mind that this shouldn’t be the main focus in this task.
Please do not reference HSBC from either the README, your code (e.g. package names), repository name etc. Happy Coding!

## Submitted task
Submitted task is [here](flickrbrowser/)